
from   __future__ import print_function

import os
import json
import sys
import traceback
import CRL
import helpers
from   helpers.io import ErrorMessage, WarningMessage
from   helpers    import trace, l, u, n
import plugins
from   Hurricane  import DbU
from   plugins.alpha.block.configuration  import IoPin, GaugeConf
from   plugins.alpha.block.iospecs        import IoSpecs
from   plugins.alpha.block.block          import Block
from   plugins.alpha.core2chip.libresocio import CoreToChip
from   plugins.alpha.chip.configuration   import ChipConf
from   plugins.alpha.chip.chip            import Chip


af  = CRL.AllianceFramework.get()

def scriptMain (**kw):
    """The mandatory function to be called by Coriolis CGT/Unicorn."""
    global af
    rvalue = True
    coreSize = 5200.0 
    chipBorder = 2*214.0 + 10*13.0
    ioSpecs = IoSpecs()
    ioSpecs.loadFromPinmux( './non_generated/litex_pinpads.json' )
    try:
        #helpers.setTraceLevel( 550 )
        cell, editor = plugins.kwParseMain( **kw )
        cell = af.getCell( 'ls180', CRL.Catalog.State.Logical )
        if cell is None:
            print( ErrorMessage( 2, 'doDesign.scriptMain(): Unable to load cell "{}".' \
                                    .format('ls180') ))
            sys.exit(1)
        if editor: editor.setCell( cell )
        ls180Conf = ChipConf( cell, ioPads=ioSpecs.ioPadsSpec )
        ls180Conf.cfg.etesian.bloat = 'nsxlib'
        ls180Conf.cfg.etesian.uniformDensity = True
        ls180Conf.cfg.etesian.aspectRatio = 1.0
        ls180Conf.cfg.etesian.spaceMargin = 0.05
        ls180Conf.cfg.anabatic.searchHalo = 2
        ls180Conf.cfg.anabatic.globalIterations = 20
        ls180Conf.cfg.anabatic.topRoutingLayer = 'METAL5'
        ls180Conf.cfg.katana.hTracksReservedLocal = 6
        ls180Conf.cfg.katana.vTracksReservedLocal = 3
        ls180Conf.cfg.katana.hTracksReservedMin = 3
        ls180Conf.cfg.katana.vTracksReservedMin = 1
        ls180Conf.cfg.block.spareSide = u(200)
        ls180Conf.editor = editor
        ls180Conf.useSpares = True
        ls180Conf.useClockTree = True
        ls180Conf.useHFNS = False
        ls180Conf.bColumns = 2
        ls180Conf.bRows = 2
        ls180Conf.chipConf.name = 'chip'
        ls180Conf.chipConf.ioPadGauge = 'LibreSOCIO'
        ls180Conf.coreSize = (u(coreSize), u(coreSize))
        ls180Conf.chipSize = (u(coreSize + chipBorder), u(coreSize + chipBorder))
        ls180ToChip = CoreToChip( ls180Conf )
        ls180ToChip.buildChip()
        chipBuilder = Chip( ls180Conf )
        rvalue = chipBuilder.doPnR()
        chipBuilder.save()
        CRL.Gds.save( ls180Conf.chip )
    except Exception, e:
        helpers.io.catch(e)
        rvalue = False
    sys.stdout.flush()
    sys.stderr.flush()
    return rvalue
