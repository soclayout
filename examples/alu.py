from nmigen import *
from nmigen.cli import rtlil


class ALU(Elaboratable):
    def __init__(self, width):
        self.sel = Signal(2)
        self.a   = Signal(width)
        self.b   = Signal(width)
        self.o   = Signal(width)
        self.co  = Signal()
        #self.m_clock   = Signal(reset_less=True)
        #self.p_reset   = Signal(reset_less=True)

    def elaborate(self, platform):
        m = Module()
        #m.domains.sync = ClockDomain()
        #m.d.comb += ClockSignal().eq(self.m_clock)

        with m.If(self.sel == 0b00):
            m.d.sync += self.o.eq(self.a | self.b)
        with m.Elif(self.sel == 0b01):
            m.d.sync += self.o.eq(self.a & self.b)
        with m.Elif(self.sel == 0b10):
            m.d.sync += self.o.eq(self.a ^ self.b)
        with m.Else():
            m.d.sync += Cat(self.o, self.co).eq(self.a - self.b)
        return m


def create_ilang(dut, ports, test_name):
    vl = rtlil.convert(dut, name=test_name, ports=ports)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    alu = ALU(width=16)
    create_ilang(alu, [alu.o, alu.a, alu.b, alu.co], "alu")

