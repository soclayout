#!/bin/bash

ALLIANCE_TOOLKIT=${ALLIANCE_TOOLKIT:-${HOME}/alliance-check-toolkit/}

echo "creating symlinks"

mkdir -p mk/dks.d
mkdir -p mk/users.d

declare -a ScriptsArray=("os" "users" "binaries" "alliance"
        "design-flow" "pr-coriolis" "pr-alliance" "pr-hibikino"
	"synthesis-yosys"
)

for script in "${ScriptsArray[@]}"; do
    if [ ! -L "mk/$script.mk" ]; then
        echo "link" mk/$script.mk
        ln -s $ALLIANCE_TOOLKIT/etc/mk/$script.mk mk/$script.mk
    fi
done

declare -a LibsArray=("sxlib" "nsxlib" "nsxlib45" "cmos" "mosis"
)

for script in "${LibsArray[@]}"; do
    if [ ! -L "mk/dks.d/$script.mk" ]; then
        echo "link" mk/dks.d/$script.mk
        ln -s $ALLIANCE_TOOLKIT/etc/mk/dks.d/$script.mk mk/dks.d/$script.mk
    fi
done

declare -a UsersArray=("lkcl"
)

for script in "${UsersArray[@]}"; do
    if [ ! -L "mk/users.d/user-$script.mk" ]; then
        echo "link" mk/users.d/user-$script.mk
        ln -s $ALLIANCE_TOOLKIT/etc/mk/users.d/user-$script.mk \
		mk/users.d/user-$script.mk
    fi
done

