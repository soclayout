from nmigen import *
from nmigen.cli import rtlil


class ADD(Elaboratable):
    def __init__(self, width):
        self.a   = Signal(width)
        self.b   = Signal(width)
        self.f   = Signal(width)

    def elaborate(self, platform):
        m = Module()
        m.d.sync += self.f.eq(self.a + self.b)
        return m


def create_ilang(dut, ports, test_name):
    vl = rtlil.convert(dut, name=test_name, ports=ports)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    alu = ADD(width=4)
    create_ilang(alu, [alu.a, alu.b, alu.f], "add")
