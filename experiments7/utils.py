# -*- coding: utf-8 -*-
from __future__ import print_function
import re

import Anabatic
import CRL
import Cfg
import Etesian
import Katana
from Hurricane import (
    Breakpoint, DbU, DataBase, UpdateSession, Box, Transformation, Instance,  Pad, Pin,
    NetExternalComponents,
)
from plugins import rsave


class SessionManager(object):
    """
    Context manager for a GO update session. See Hurricane reference manual
    for an info on Hurricane::UpdateSession class.
    """

    def __enter__(self):
        UpdateSession.open()

    def __exit__(self, *args):
        UpdateSession.close()


class Module(object):

    _layer_cache = {}
    _instances = None

    def __init__(self, cell, editor, width=None, height=None, submodules=None,
                 pin_width=2.0, pin_height=2.0, pin_suffix='.0',
                 pin_layer=None, north_pins=None, east_pins=None,
                 south_pins=None, west_pins=None, pads=None,
                 orientation=None, **kwargs):
        """
        Creates a module.

        :param cell: cell name or Hurricane.Cell object,
        :param editor: editor object when executing from cgt (or None),
        :param width: module width,
        :param height: module height,
        :param submodules: submodules (Module objects)
          or tuples of (submodule, x, y), where (x, y) is a submodule's
          placement point in lambdas,
        :param pin_width: default pin width,
        :param pin_height: default pin height,
        :param pin_suffix: default pin suffix,
        :param pin_layer: default layer for placing pins,
        :param north_pins: list of pin configuration dictionaries for placing
          pins on the north side,
        :param east_pins: ditto (for the east side),
        :param south_pins: ditto (for the south side),
        :param west_pins: ditto (for the west side),
        :param pads: dictionary of {net: list of layers} for creating pads,
        :param orientation: when placed, should be orientated/mirrored etc.
        :param kwargs: extra parameters to be implemented in derived classes.
        """
        self.editor = editor
        self.af = CRL.AllianceFramework.get()
        self.db = DataBase.getDB()
        self.width = width
        self.height = height

        if isinstance(cell, basestring):
            self.cell = self.af.getCell(cell, CRL.Catalog.State.Logical)
        else:
            self.cell = cell

        self.pin_width = pin_width
        self.pin_height = pin_height
        self.pin_suffix = pin_suffix
        if pin_layer is None:
            self.pin_layer = self.get_layer('METAL3')
        elif isinstance(pin_layer, basestring):
            self.pin_layer = self.get_layer(pin_layer)
        else:
            self.pin_layer = pin_layer
        self.north_pins = north_pins or []
        self.east_pins = east_pins or []
        self.south_pins = south_pins or []
        self.west_pins = west_pins or []

        self.orientation = orientation or Transformation.Orientation.ID

        self.pads = pads or {}

        self._submodules = []
        if submodules is not None:
            for submodule in submodules:
                self._submodules.append(
                    (submodule, None, None) if isinstance(submodule, Module)
                    else submodule
                )


    @property
    def name(self):
        return self.cell.getName()

    @name.setter
    def name(self, name):
        self.cell.setName(name)

    def __str__(self):
        return self.name

    @property
    def ab(self):
        """ The real abutment box. """
        return self.cell.getAbutmentBox()

    @ab.setter
    def ab(self, ab):
        self.cell.setAbutmentBox(ab)

    @property
    def instances(self):
        """ Cached instances. """
        if self._instances is None:
            self._instances = self.cell.getInstances()

        return self._instances

    def get_layer(self, name):
        """ Creates a new layer or returns it from cache. """
        if name in self._layer_cache:
            return self._layer_cache[name]

        layer = self.db.getTechnology().getLayer(name)
        self._layer_cache[name] = layer
        return layer

    @staticmethod
    def to_dbu(lmb):
        """
        Convert lambdas to database units. (See Hurricane+Python Manual 3.4.)
        """
        return DbU.fromLambda(lmb)

    @staticmethod
    def from_dbu(dbu):
        """
        Convert lambdas to database units. (See Hurricane+Python Manual 3.4.)
        """
        return DbU.toLambda(dbu)

    @property
    def ab_x(self):
        return self.from_dbu(self.ab.getXMin())

    @property
    def ab_y(self):
        return self.from_dbu(self.ab.getYMin())

    @property
    def ab_width(self):
        return self.from_dbu(self.ab.getWidth())

    @property
    def ab_height(self):
        return self.from_dbu(self.ab.getXHeight())

    @staticmethod
    def match_instance(datapath_insts, op, plug_name, inst):
        """
        Guess the position of an instance from its nets connections,
        and put it at the right place in the datapath vector.

        :param datapath_insts: vector of bit slices,
        :param op: operator name,
        :param inst: instance to classify,
        :param plug_name: name of the plug to use to guess the bit index,
        :return: boolean, True if the instance has been matched.
        """
        if not inst.getMasterCell().getName().startswith(op):
            return False
        re_net_index = re.compile(r'[^(]+\((?P<index>[\d]+)\)$')
        for plug in inst.getPlugs():
            if plug.getMasterNet().getName() != plug_name:
                continue
            m = re_net_index.match(plug.getNet().getName())
            if not m:
                continue
            bit_slice = datapath_insts[int(m.group('index'))]
            for column in bit_slice:
                if column[0] == op:
                    column[1] = inst
                    print ("match", plug_name, int(m.group('index')),
                            column, inst)
                    return True
            break
        return False

    def compute_ab(self):
        """ Compute default abutment box without placement. """
        etesian = Etesian.EtesianEngine.create(self.cell)
        etesian.setDefaultAb()
        etesian.destroy()

    def set_ab(self, width, height):
        """
        Let the user specify the abutment box. Bottom left corner always
        sets to (0,0). 
        """
        self.cell.setAbutmentBox(Box(self.to_dbu(0.0), self.to_dbu(0.0),
                                     self.to_dbu(width), self.to_dbu(height)))

    def place(self):
        """ Places the current cell. """
        etesian = Etesian.EtesianEngine.create(self.cell)
        etesian.place()

    def route(self):
        """ Routes the current cell. """
        katana = Katana.KatanaEngine.create(self.cell)
        katana.digitalInit()
        katana.runGlobalRouter(Katana.Flags.NoFlags)
        katana.loadGlobalRouting(Anabatic.EngineLoadGrByNet)
        katana.layerAssign(Anabatic.EngineNoNetLayerAssign)
        katana.runNegociate(Katana.Flags.NoFlags)
        #Breakpoint.stop(0, 'After routing {0}'.format(self.cell))
        katana.finalizeLayout()
        result = katana.isDetailedRoutingSuccess()
        katana.destroy()
        return result

    def place_and_route(self):
        """ Places and routes. """
        self.place()
        return self.route()

    def create_pin_series(self, net, direction, name=None,
                          status=Pin.PlacementStatus.FIXED, layer=None,
                          x=None, y=None, width=2.0, height=2.0,
                          repeat=1, delta=0.0, external=True):
        """
        Creates a pin or a series of pins in a cell.

        :param net: Hurricane.Net object name or name template, taking a pin
          enumeration parameter, i. e. pin number,
        :param name: pin name or name template taking a pin enumeration
          parameter (net name or template + suffix),
        :param direction: Pin.Direction value,
        :param status: Pin.PlacementStatus value (default is FIXED),
        :param layer: Hurricane.Layer object or name (METAL3),
        :param x: starting pin position (left to right, 0.0),
        :param y: starting pin position (bottom to top, 0.0),
        :param width: pin width (2,0),
        :param height: pin height (2.0),
        :param repeat: a number of pins to be placed or an iterable containing
          pin template parameters (i. e. pin number, 1),
        :param delta: next pin position offset (0.0),
        :param external: mark pin as external (yes),
        :return: tuple of next pin coordinates, or just (x, y), if delta
          was not provided.
        """
        if layer is None:
            layer = self.pin_layer
        elif isinstance(layer, basestring):
            layer = self.get_layer(layer)

        if isinstance(repeat, int):
            if repeat > 1 and delta == 0.0:
                raise Warning(
                    '{}: you are trying to place pins on each other.'.format(
                        self.name
                    )
                )
            iterator = range(repeat)
        else:
            iterator = repeat

        if name is None:
            name = net + self.pin_suffix

        for i in iterator:
            pin = Pin.create(self.cell.getNet(net.format(i)),
                             name.format(i), direction, status, layer,
                             self.to_dbu(x), self.to_dbu(y),
                             self.to_dbu(width), self.to_dbu(height))
            if direction in (Pin.Direction.NORTH, Pin.Direction.SOUTH):
                x += delta
            else:
                # EAST or WEST
                y += delta

            if external:
                pin.getNet().setExternal(True)
                NetExternalComponents.setExternal(pin)

        return x, y

    def init_ab(self):
        """ Create the abutment box with object's initial values. """
        if self.width and self.height:
            self.ab = Box(
                0, 0, self.to_dbu(self.width), self.to_dbu(self.height)
            )
        else:
            raise Warning('{}: Module size is not set.'.format(self.name))

    def create_pins(self):
        """ Creates all pins set on Module object creation. """

        def default_x():
            if direction == Pin.Direction.EAST:
                return self.from_dbu(self.ab.getWidth())
            if direction == Pin.Direction.WEST:
                return 0
            return last_x

        def default_y():
            if direction == Pin.Direction.NORTH:
                return self.from_dbu(self.ab.getHeight())
            if direction == Pin.Direction.SOUTH:
                return 0
            return last_y

        for pins, direction in (
            (self.north_pins, Pin.Direction.NORTH),
            (self.east_pins, Pin.Direction.EAST),
            (self.south_pins, Pin.Direction.SOUTH),
            (self.west_pins, Pin.Direction.WEST),
        ):
            last_x = last_y = 0.0
            for pin_config in pins:
                net = pin_config.pop('net')
                name = pin_config.pop('name', net + self.pin_suffix)
                layer = pin_config.pop('layer', self.pin_layer)
                x = pin_config.pop('x', default_x())
                y = pin_config.pop('y', default_y())
                last_x, last_y = self.create_pin_series(
                    net, direction, name=name, layer=layer, x=x, y=y,
                    **pin_config)

    def create_pads_for_net(self, net, layers):
        """
        Creates a series of pads for a given net.

        :param net: net name or Hurricane.Net object to create pads for,
        :param layers: list of layer names or Hurricane.Layer objects
          to create pads on.
        """
        temp_ab = self.ab
        temp_ab.inflate(self.to_dbu(-5.0))
        if isinstance(net, basestring):
            net = self.cell.getNet(net)

        for layer in layers:
            if isinstance(layer, basestring):
                layer = self.get_layer(layer)
            Pad.create(net, layer, temp_ab)

    def create_pads(self):
        """ Create all pads for a given Module object. """

        for net, layers in self.pads.items():
            self.create_pads_for_net(net, layers)

    @property
    def submodules(self):
        """ Submodules iterator. """
        return iter(submodule for submodule, x, y in self._submodules)

    def find_submodule(self, name):
        """
        Returns first submodule matching `name`. Better give your submodules
        unique names.

        :param name: submodule name to match,
        :return: `Module` object.
        """
        return next(s for s in self.submodules if s.name == name)

    def build_submodules(self):
        """
        Execute submodules and gather their status.

        :return: True if all submodules executed successfully, False otherwise.
        """

        for submodule in self.submodules:
            if not submodule.build():
                return False

        return True

    def place_submodule(self, submodule, x, y):
        """
        Places a submodule to a given location.

        :param submodule: `Module` object,
        :param x: placement coordinate,
        :param y: placement coordinate.
        """

        # find instance
        instance = [
            inst for inst in self.instances if inst.getName().endswith(
                submodule.name
            )
        ][0]

        # place submodule
        instance.setTransformation(Transformation(
            self.to_dbu(x), self.to_dbu(y), submodule.orientation,
        ))
        instance.setPlacementStatus(Instance.PlacementStatus.FIXED)

    def place_submodules(self):
        """
        Places the submodules in the current module using their initial
        placement points, if set.
        """
        for submodule, x, y in self._submodules:
            if x is not None and y is not None:
                self.place_submodule(submodule, x, y)
            else:
                raise Warning((
                    '{}: cannot place {}, because its '
                    'initial placement point is not set.'
                ).format(self.name, submodule.name))

    def save(self):
        """ Saves cell. """
        rsave.scriptMain(editor=self.editor, cell=self.cell)

    def build(self):
        """ Main routine. """

        raise NotImplementedError('You need to implement the `build` method.')

    def get_net_connections(self, to_find, already_found):
        inst = self.cell.getInstances()
        return get_net_connections(inst, to_find, already_found)


class Config:

    def __init__(self, priority=None):
        self._priority = priority

    def __enter__(self):
        if self._priority is not None:
            Cfg.Configuration.pushDefaultPriority(self._priority)
        return self

    def __setattr__(self, attr, val):
        if attr.startswith("_"):
            self.__dict__[attr] = val
            return
        attr = attr.replace("_", ".")
        if isinstance(val, bool):
            Cfg.getParamBool(attr).setBool(val)
        elif isinstance(val, int):
            p = Cfg.getParamInt(attr) # all params have a type
            if p.type == 'Enumerate':
                Cfg.getParamEnumerate(attr).setInt(val)
            else:
                Cfg.getParamInt(attr).setInt(val)
        elif '%' in val:
            Cfg.getParamPercentage(attr).setPercentage(float(val[:-1]))
        else:
            Cfg.getParamString(attr).setString(val)

    def __exit__(self, *args):
        if self._priority is not None:
            Cfg.Configuration.popDefaultPriority()


def get_net_connections(instances, find, already_found):
    res = set()
    new = set()
    search_more = []
    for inst in instances:
        if (inst.getPlacementStatus() !=
                Instance.PlacementStatus.UNPLACED):
            continue
        #print ("instance", inst)
        for plug in inst.getConnectedPlugs():
            netname = plug.getNet().getName()
            if netname in already_found:
                continue
            if plug.getNet().getName() in find:
                #print ("plug", plug, plug.getNet().getName())
                for p in plug.getNet().getPlugs():
                    c = p.getInstance()
                    if (c.getPlacementStatus() !=
                            Instance.PlacementStatus.UNPLACED):
                        continue
                    #print ("notplaced", c)
                    for pc in c.getConnectedPlugs():
                        #print ("plug", pc)
                        pn = pc.getNet().getName()
                        if pn not in find and pn not in already_found:
                            search_more.append(pn)
                    new.add(c)
    res.update(new)
    if search_more:
        print("more", search_more)
        new_found = find + already_found
        more = get_net_connections(new, search_more, new_found)
        res.update(more)

    return res
