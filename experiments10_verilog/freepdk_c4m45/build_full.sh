#!/bin/sh

# full core build including QTY 4of 4k SRAMs: please remember to alter
# doDesign.py before running!
# change the settings to the larger chip/corona size
echo "remember to check doDesign core size"

# initialise/update the pinmux & c4m-pdk-freepdk45 submodule
#pushd ..
git submodule update --init --remote
#popd

# makes symlinks to alliance
./mksym.sh
touch mk/users.d/user-${USER}.mk

# clear out
make clean
rm *.vst *.ap *.v

# make the vst from ilang
make vst

# starts the build.
make lvx

