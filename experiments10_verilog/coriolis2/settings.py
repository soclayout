# -*- Mode:Python -*-

from __future__ import print_function
import os
import Viewer
#import node180.scn6m_deep_09
from   helpers       import overlay, l, u, n

import os
import socket
import helpers

import Cfg
from   Hurricane import DataBase, Cell, Instance, Net
from   CRL     import AllianceFramework, RoutingLayerGauge
import CRL
import Viewer
from   helpers.overlay import CfgCache
import symbolic.cmos45  # do not remove

NdaDirectory = None
if os.environ.has_key('NDA_TOP'):
    NdaDirectory = os.environ['NDA_TOP']
if not NdaDirectory:
    hostname = socket.gethostname()
    if hostname.startswith('lepka'):
        NdaDirectory = '/dsk/l1/jpc/crypted/soc/techno'
        if not os.path.isdir(NdaDirectory):
            print ('[ERROR] You forgot to mount the NDA encrypted directory, stupid!')
    else:
        NdaDirectory = '/users/soft/techno/techno'
helpers.setNdaTopDir( NdaDirectory )

# select one or other of these
if False:
    from   NDA.node45.freepdk45_c4m import techno, FlexLib, LibreSOCIO
    techno.setup()
    FlexLib.setup()
    LibreSOCIO.setup()
else:
    import symbolic.cmos45
    import pll # fake pll
    pll.setup()


if os.environ.has_key('CELLS_TOP'):
    cellsTop = os.environ['CELLS_TOP']
else:
    cellsTop = '../../../alliance-check-toolkit/cells'

db = DataBase.getDB()
af = AllianceFramework.get()


def createPLLBlackbox ():
    global db, af
    print( '  o  Creating PLL blackboxes for "ls180" design.' )
    rootlib  = db.getRootLibrary()
    lib      = rootlib.getLibrary( 'pll' )
    pllName = 'pll'
    pll     = lib.getCell( pllName )
    if not pll:
        raise ErrorMessage( 1, 'settings.createPLLBlackBox(): '
                                'PLL Cell "{}" not found.' \
                               .format(pllName) )
    pll.setAbstractedSupply( True )
    blackboxName = 'real_pll'
    cell     = Cell.create( lib, blackboxName )
    instance = Instance.create( cell, 'real_pll', pll )
    state    = af.getCatalog().getState( blackboxName, True )
    state.setCell( cell )
    state.setLogical( True )
    state.setInMemory( True )
    print( '     - {}.'.format(cell) )
    for masterNet in pll.getNets():
        if not masterNet.isExternal():
            continue
        net = Net.create( cell, masterNet.getName() )
        net.setDirection( masterNet.getDirection() )
        net.setType( masterNet.getType() )
        net.setExternal( True )
        net.setGlobal( masterNet.isGlobal() )
        if masterNet.isSupply():
            continue
        plug = instance.getPlug( masterNet )
        plug.setNet( net )


with overlay.CfgCache(priority=Cfg.Parameter.Priority.UserFile) as cfg:
    cfg.misc.catchCore = False
    cfg.misc.info = False
    cfg.misc.paranoid = False
    cfg.misc.bug = False
    cfg.misc.logMode = True
    cfg.misc.verboseLevel1 = True
    cfg.misc.verboseLevel2 = True
    cfg.etesian.graphics = 3
    cfg.etesian.spaceMargin = 0.05
    cfg.etesian.aspectRatio = 1.0
    cfg.anabatic.edgeLenght = 24
    cfg.anabatic.edgeWidth = 8
    cfg.anabatic.topRoutingLayer = 'METAL5'
    cfg.katana.eventsLimit = 4000000
    cfg.etesian.effort = 2
    cfg.etesian.uniformDensity = True
    cfg.katana.hTracksReservedLocal = 7
    cfg.katana.vTracksReservedLocal = 6
    Viewer.Graphics.setStyle( 'Alliance.Classic [black]' )
    af  = CRL.AllianceFramework.get()
    env = af.getEnvironment()
    env.setCLOCK( '^clk$|^ck|^jtag_tck$' )
    env.setPOWER( 'vdd' )
    env.setGROUND( 'vss' )
    env.addSYSTEM_LIBRARY( library=cellsTop+'/niolib',
                           mode=CRL.Environment.Prepend )
    env.addSYSTEM_LIBRARY( library=cellsTop+'/nsxlib',
                           mode=CRL.Environment.Prepend )

# runs the "fake" pll blackbox create
with overlay.UpdateSession():
    createPLLBlackbox()

print( '  o  Successfully run "<>/coriolis2/settings.py".' )
print( '     - CELLS_TOP = "{}"'.format(cellsTop) )

