#!/bin/sh

# full core build including QTY 4of 4k SRAMs: please remember to alter
# doDesign.py before running!
# change the settings to the larger chip/corona size
#
# also contains Staf's manually re-connected PLL edits to the verilog
# see  commits 24cbbcc and 227a0f69
#
echo "remember to check doDesign core size"
echo "also use yosys 049e3abf9"

# fetch GDS-II files
echo "fetching GDS-II files"
wget http://ftp.libre-soc.org/C4MLogo.gds
wget http://ftp.libre-soc.org/lip6.gds
wget http://ftp.libre-soc.org/sorbonne_logo.gds
wget http://ftp.libre-soc.org/libresoc_logo.gds

# initialise/update the pinmux submodule
git submodule update --init --recursive

# makes symlinks to alliance
./mksyms.sh

# generates the io pads needed for ioring.py
make pinmux

# clear out
make clean
rm *.vst *.ap *.blif *.gds

# copies over a "full" core
#cp non_generated/full_core_4_4ksram_ls180.il ls180.il
cp non_generated/full_core_4_4ksram_ls180.v ls180.v
cp non_generated/full_core_4_4ksram_litex_ls180_recon.v litex_ls180.v
cp non_generated/full_core_4_4ksram_libresoc_recon.v libresoc.v
cp non_generated/spblock*.v .
cp non_generated/spblock*.vbe .
cp non_generated/pll.v .
touch mem.init
touch mem_1.init
touch mem_2.init
touch mem_3.init
touch mem_4.init
touch mem_5.init

# make the vst from verilog
make vst

# starts the build.
make lvx

