#!/bin/sh

# comment about yosys version needed
echo "this only works at the moment with yosys 049e3abf9"

# full core build: please remember to alter ioring.py before running!
# change the settings to the larger chip/corona size

# initialise/update the pinmux submodule
git submodule update --init --recursive

# makes symlinks to alliance
./mksyms.sh

# generates the io pads needed for ioring.py
make pinmux

# clear out
make clean
rm *.vst *.ap

# copies over a "full" core
#cp non_generated/full_core_ls180.il ls180.il
cp non_generated/ls180.v ls180.v
cp non_generated/litex_ls180.v litex_ls180.v
cp non_generated/libresoc.v libresoc.v
touch mem.init
touch mem_1.init
touch mem_2.init
touch mem_3.init
touch mem_4.init
touch mem_5.init

# make the vst from ilang
make vst

# starts the build.
make lvx

