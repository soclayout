(* blackbox = 1 *)
module pll(ref_v, div_out_test, a0, a1, vco_test_ana, out_v);
  input a0;
  input a1;
  output div_out_test;
  output out_v;
  input ref_v;
  output vco_test_ana;
endmodule

