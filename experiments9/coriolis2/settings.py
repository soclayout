# -*- Mode:Python -*-

import Cfg
from   Hurricane import DataBase, Cell, Instance, Net
from   CRL     import AllianceFramework, RoutingLayerGauge
import CRL
import Viewer
from   helpers.overlay import CfgCache
from   helpers   import overlay, l, u, n
import symbolic.cmos45  # do not remove
import os

import LibreSOCMem, pll
LibreSOCMem.setup()
pll.setup()

if 'CELLS_TOP' in os.environ:
    cellsTop = os.environ['CELLS_TOP']
else:
    cellsTop = '../../../alliance-check-toolkit/cells'

db = DataBase.getDB()
af = AllianceFramework.get()


with CfgCache('', priority=Cfg.Parameter.Priority.UserFile) as cfg:
    cfg.misc.catchCore               = False
    cfg.misc.info                    = False
    cfg.misc.paranoid                = False
    cfg.misc.bug                     = False
    cfg.misc.logMode                 = True
    cfg.misc.verboseLevel1           = True
    cfg.misc.verboseLevel2           = True
    cfg.anabatic.edgeLenght          = 24
    cfg.anabatic.edgeWidth           = 8
    cfg.anabatic.topRoutingLayer     = 'METAL5'
    cfg.etesian.graphics             = 3

    #cfg.katana.searchHalo           = 30
    #cfg.katana.eventsLimit          = 1000000
    #cfg.katana.hTracksReservedLocal = 7

    # Run 2 (make-cgt-2.log)
    #cfg.etesian.effort              = 2
    #cfg.etesian.uniformDensity      = True
    #cfg.etesian.spaceMargin         = 0.05
    #cfg.etesian.aspectRatio         = 1.0
    #cfg.katana.vTracksReservedLocal = 4
    #cfg.katana.hTracksReservedLocal = 4

    # Run 3 (make-cgt-3.log)
    #cfg.etesian.effort              = 2
    #cfg.etesian.uniformDensity      = False
    #cfg.etesian.spaceMargin         = 0.05
    #cfg.etesian.aspectRatio         = 1.0
    #cfg.katana.vTracksReservedLocal = 5
    #cfg.katana.hTracksReservedLocal = 5

    # Run 4 (make-cgt-4.log)
    #cfg.etesian.effort              = 2
    #cfg.etesian.uniformDensity      = True
    #cfg.etesian.spaceMargin         = 0.05
    #cfg.etesian.aspectRatio         = 1.0

    # Run 5 (make-cgt-5.log)
    #cfg.etesian.effort               = 2
    #cfg.etesian.uniformDensity       = True
    #cfg.etesian.spaceMargin          = 0.05
    #cfg.etesian.aspectRatio          = 1.0

    # Run 6 (make-cgt-6.log) copy from benchs/6502/cmos45/coriolos2/settings
    cfg.etesian.effort               = 2
    cfg.etesian.uniformDensity       = True
    cfg.etesian.spaceMargin          = 0.3
    cfg.etesian.aspectRatio          = 1.0
    cfg.etesian.bloat                = 'nsxlib'
    cfg.katana.searchHalo            = 4

    cfg.katana.useGlobalEstimate     = False
    cfg.katana.vTracksReservedLocal  = 7
    cfg.katana.hTracksReservedLocal  = 6
    cfg.katana.bloatOverloadAdd      = 4
    cfg.conductor.stopLevel          = 0
    cfg.conductor.maxPlaceIterations = 2
    cfg.conductor.useFixedAbHeight   = False

    env = CRL.AllianceFramework.get().getEnvironment()
    env.setCLOCK( '^ck|^pll_clk|^jtag_tck$' )
    env.addSYSTEM_LIBRARY(library=cellsTop+'/niolib',
                           mode=CRL.Environment.Prepend )
    env.addSYSTEM_LIBRARY(library=cellsTop+'/nsxlib',
                          mode=CRL.Environment.Prepend )
    env.setPOWER ('vdd')
    env.setGROUND('vss')

Viewer.Graphics.setStyle('Alliance.Classic [black]')

print( '  o  Successfully run "<>/coriolis2/settings.py".' )
print( '     - CELLS_TOP = "{}"'.format(cellsTop) )

