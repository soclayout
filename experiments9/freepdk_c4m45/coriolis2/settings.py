# -*- Mode:Python -*-

from   __future__ import print_function
import os
import socket
import helpers

NdaDirectory = None
if os.environ.has_key('NDA_TOP'):
    NdaDirectory = os.environ['NDA_TOP']
if not NdaDirectory:
    hostname = socket.gethostname()
    if hostname.startswith('lepka'):
        NdaDirectory = '/dsk/l1/jpc/crypted/soc/techno'
        if not os.path.isdir(NdaDirectory):
            print ('[ERROR] You forgot to mount the NDA '
                   'encrypted directory, stupid!')
    else:
        NdaDirectory = '/users/soft/techno/techno'
helpers.setNdaTopDir( NdaDirectory )

import Cfg
from   Hurricane import DataBase, Cell, Instance, Net
from   CRL     import AllianceFramework, RoutingLayerGauge
from   helpers   import overlay, l, u, n
from   helpers import overlay, l, u, n
from   NDA.node45.freepdk45_c4m import techno, FlexLib, LibreSOCIO

# "fake" 4k SRAM
import LibreSOCMem

techno.setup()
FlexLib.setup()
LibreSOCIO.setup()
LibreSOCMem.setup()


db = DataBase.getDB()
af = AllianceFramework.get()


def createSramBlackbox ():
    global db, af
    print( '  o  Creating SRAM blackboxes for "ls180" design.' )
    rootlib  = db.getRootLibrary()
    lib      = rootlib.getLibrary( 'LibreSOCMem' )
    sramName = 'spblock_512w64b8w'
    sram     = lib.getCell( sramName )
    if not sram:
        raise ErrorMessage( 1, 'settings.createSramBlocks(): '
                                'SRAM Cell "{}" not found.' \
                               .format(sramName) )
    sram.setAbstractedSupply( True )
    blackboxeNames = [ 'spblock_512w64b8w_0',
                       'spblock_512w64b8w_1',
                       'spblock_512w64b8w_2',
                       'spblock_512w64b8w_3',
                     ]
    for blackboxName in blackboxeNames:
        cell     = Cell.create( lib, blackboxName )
        instance = Instance.create( cell, 'real_sram', sram )
        state    = af.getCatalog().getState( blackboxName, True )
        state.setCell( cell )
        state.setLogical( True )
        state.setInMemory( True )
        print( '     - {}.'.format(cell) )
        for masterNet in sram.getNets():
            if not masterNet.isExternal():
                continue
            net = Net.create( cell, masterNet.getName() )
            net.setDirection( masterNet.getDirection() )
            net.setType( masterNet.getType() )
            net.setExternal( True )
            net.setGlobal( masterNet.isGlobal() )
            if masterNet.isSupply():
                continue
            plug = instance.getPlug( masterNet )
            plug.setNet( net )


with overlay.CfgCache(priority=Cfg.Parameter.Priority.UserFile) as cfg:
    cfg.misc.catchCore           = False
    cfg.misc.minTraceLevel       = 12300
    cfg.misc.maxTraceLevel       = 12400
    cfg.misc.info                = False
    cfg.misc.paranoid            = False
    cfg.misc.bug                 = False
    cfg.misc.logMode             = True
    cfg.misc.verboseLevel1       = True
    cfg.misc.verboseLevel2       = True
    cfg.etesian.graphics         = 3
    cfg.etesian.spaceMargin      = 0.10
    cfg.anabatic.topRoutingLayer = 'metal6'
    cfg.katana.eventsLimit       = 4000000
    af  = AllianceFramework.get()
    lg5 = af.getRoutingGauge('FlexLib').getLayerGauge( 5 )
    lg5.setType( RoutingLayerGauge.PowerSupply )
    env = af.getEnvironment()
    env.setCLOCK( '^sys_clk$|^ck|^jtag_tck$' )

# XXX cannot run this in non-NDA'd mode because there is no
# NDA.node45.freepdk45_c4m.LibreSOCMem (or PLL)
# TODO: create a fake one
with overlay.UpdateSession():
    createSramBlackbox()
