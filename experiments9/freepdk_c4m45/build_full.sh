#!/bin/sh

# full core build including QTY 4of 4k SRAMs: please remember to alter
# doDesign.py before running!
# change the settings to the larger chip/corona size
echo "remember to check doDesign core size"

# initialise/update the pinmux & c4m-pdk-freepdk45 submodule
#pushd ..
git submodule update --init --recursive
#popd

# makes symlinks to alliance
../mksym.sh
touch mk/users.d/user-${USER}.mk

# generates the io pads needed for ioring.py
make pinmux

# clear out
make clean
rm *.vst *.ap

# copies over a "full" core
#cp non_generated/full_core_ls180.il ls180.il
cp non_generated/full_core_4_4ksram_ls180.v ls180.v
cp non_generated/full_core_4_4ksram_litex_ls180.v litex_ls180.v
cp non_generated/libresoc.v libresoc.v
cp non_generated/spblock*.v .
cp non_generated/spblock*.vbe .
cp non_generated/pll.v .
touch mem.init
touch mem_1.init
touch mem_2.init
touch mem_3.init
touch mem_4.init
touch mem_5.init

# make the vst from ilang
make vst

# starts the build.
make layout # not "make vst!" this is for "real" (GDS-II) not AP

