
import CRL
import Hurricane
import Viewer
import Cfg
from   Hurricane import Technology, DataBase, DbU, Library,     \
                        Layer, BasicLayer, Cell, Net, Vertical, \
                        Rectilinear, Box, Point, Instance,      \
                        Transformation, NetExternalComponents,  \
                        Horizontal, Pad
from helpers            import u, l
from helpers.overlay    import UpdateSession


__all__ = [ "setup" ]


def _load():
    print( '  o  pll.py: Create dummy abstract of "pll".' ) 

    af          = CRL.AllianceFramework.get()
    rg          = af.getRoutingGauge()
    cg          = af.getCellGauge()
    db          = DataBase.getDB()
    tech        = db.getTechnology()
    rootlib     = db.getRootLibrary()
    sliceHeight = cg.getSliceHeight()
    METAL2      = tech.getLayer( 'METAL2' )
    METAL3      = tech.getLayer( 'METAL3' )
    METAL4      = tech.getLayer( 'METAL4' )
    METAL5      = tech.getLayer( 'METAL5' )
    BLOCKAGE1   = tech.getLayer( 'BLOCKAGE1' )
    BLOCKAGE2   = tech.getLayer( 'BLOCKAGE2' )
    BLOCKAGE3   = tech.getLayer( 'BLOCKAGE3' )
    BLOCKAGE4   = tech.getLayer( 'BLOCKAGE4' )
    BLOCKAGE5   = tech.getLayer( 'BLOCKAGE5' )

    with UpdateSession():
        lib  = Library.create( rootlib, 'pll' )
        cell = Cell.create( lib, 'pll' )

        cell.setAbutmentBox( Box( 0, 0, (15+2)*sliceHeight, (13+2)*sliceHeight ) )
        nets     = {}
        netSpecs = ( ( 'a0'          , None           , Net.Direction.IN , True )
                   , ( 'a1'          , None           , Net.Direction.IN , True )
                   , ( 'ref_v'       , None           , Net.Direction.IN , True )
                   , ( 'out_v'       , None           , Net.Direction.OUT, True )
                   , ( 'vco_test_ana', None           , Net.Direction.OUT, True )
                   , ( 'div_out_test', None           , Net.Direction.OUT, True )
                   , ( 'vdd'         , Net.Type.POWER , Net.Direction.IN , True )
                   , ( 'vss'         , Net.Type.GROUND, Net.Direction.IN , True )
                   , ( 'blockageNet' , None           , Net.Direction.IN , True )
                   )
        for name, netType, direction, external in netSpecs:
            nets[ name ] = Net.create( cell, name )
            if netType   is not None: nets[name].setType     ( netType )
            if direction is not None: nets[name].setDirection( direction )
            if external  is not None: nets[name].setExternal ( external )
        blockageArea = Box( cell.getAbutmentBox() )
        blockageArea.inflate( -sliceHeight )
        for layer in (BLOCKAGE1, BLOCKAGE2, BLOCKAGE3, BLOCKAGE4):
            Pad.create( nets['blockageNet'], layer, blockageArea )
        METAL2pitch   = rg.getLayerGauge( METAL2 ).getPitch()
        METAL3pitch   = rg.getLayerGauge( METAL3 ).getPitch()
        METAL2width   = rg.getLayerGauge( METAL2 ).getWireWidth()
        METAL3width   = rg.getLayerGauge( METAL3 ).getWireWidth()

        xref = cell.getAbutmentBox().getXMin() + sliceHeight
        yref = cell.getAbutmentBox().getYMax() - sliceHeight + METAL2pitch
        for name, xpitchs in (('a0',20), ('ref_v',75), ('a1',130)):
            segment = Vertical.create( nets[name]
                                     , METAL3
                                     , xref + xpitchs*METAL3pitch
                                     , METAL3width
                                     , yref
                                     , yref - sliceHeight )
            segment = Vertical.create( nets[name]
                                     , METAL3
                                     , xref + xpitchs*METAL3pitch
                                     , METAL3width
                                     , yref
                                     , yref - METAL2pitch )
            NetExternalComponents.setExternal( segment )

        xref = cell.getAbutmentBox().getXMin() + sliceHeight - METAL3pitch
        yref = cell.getAbutmentBox().getYMin() + sliceHeight
        for name, ypitchs in (('vco_test_ana',65), ('div_out_test',120)):
            segment = Horizontal.create( nets[name]
                                       , METAL2
                                       , yref + ypitchs*METAL2pitch
                                       , METAL2width
                                       , xref
                                       , xref + sliceHeight )
            segment = Horizontal.create( nets[name]
                                       , METAL2
                                       , yref + ypitchs*METAL2pitch
                                       , METAL2width
                                       , xref
                                       , xref + METAL3pitch )
            NetExternalComponents.setExternal( segment )

        xref = cell.getAbutmentBox().getXMax() - 2*sliceHeight + METAL3pitch
        yref = cell.getAbutmentBox().getYMin() +  sliceHeight
        for name, ypitchs in (('out_v',120),):
            segment = Horizontal.create( nets[name]
                                       , METAL2
                                       , yref + ypitchs*METAL2pitch
                                       , METAL2width
                                       , xref
                                       , xref + sliceHeight )
            segment = Horizontal.create( nets[name]
                                       , METAL2
                                       , yref + ypitchs*METAL2pitch
                                       , METAL2width
                                       , xref + sliceHeight
                                       , xref + sliceHeight - METAL3pitch )
            NetExternalComponents.setExternal( segment )

        xmin = cell.getAbutmentBox().getXMin() +   sliceHeight
        xmax = cell.getAbutmentBox().getXMax() -   sliceHeight
        yref = cell.getAbutmentBox().getYMin() + 2*sliceHeight
        for i in range(4):
            net = nets['vdd']
            if i % 2:
                net = nets['vss']
            segment = Horizontal.create( net
                                       , METAL5
                                       , yref + sliceHeight*3*i
                                       , 2*sliceHeight
                                       , xmin
                                       , xmax )
            NetExternalComponents.setExternal( segment )
            
    af.wrapLibrary( lib, 0 )

    return lib

def setup():
    lib = _load()
    return lib
