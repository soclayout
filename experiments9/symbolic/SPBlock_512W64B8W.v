(* blackbox = 1 *)
module SPBlock_512W64B8W(input [8:0] a,
			 input [63:0] d,
			 output [63:0] q,
			 input [7:0] we,
			 input clk);
endmodule // SPBlock_512W64B8W
