# auto-generated by Libre-SOC pinmux program: do not edit
# python src/pinmux_generator.py -v -s {spec} -o {output}
pindict = {
    'mspi1': [ 'ck+', 'nss+', 'mosi+', 'miso-', ],
    'mspi0': [ 'ck+', 'nss+', 'mosi+', 'miso-', ],
    'vss': [ '0-', '1-', '2-', '3-', 
                '4-', ],
    'vdd': [ '0-', '1-', '2-', '3-', 
                '4-', ],
    'pwm': [ '0+', '1+', ],
    'jtag': [ 'tms-', 'tdi-', 'tdo+', 'tck+', ],
    'vdd': [ '0-', '1-', '2-', ],
    'sys': [ 'clk-', 'rst-', 'pllclk-', 'pllout+', 
                'csel0-', 'csel1-', 'pllock+', ],
    'uart0': [ 'tx+', 'rx-', ],
    'vss': [ '0-', '1-', '2-', ],
    'sdr': [ 'dqm0+', 'd0*', 'd1*', 'd2*', 
                'd3*', 'd4*', 'd5*', 'd6*', 
                'd7*', 'ad0+', 'ad1+', 'ad2+', 
                'ad3+', 'ad4+', 'ad5+', 'ad6+', 
                'ad7+', 'ad8+', 'ad9+', 'ba0+', 
                'ba1+', 'clk+', 'cke+', 'rasn+', 
                'casn+', 'wen+', 'csn0+', 'ad10+', 
                'ad11+', 'ad12+', 'dqm1+', 'd8*', 
                'd9*', 'd10*', 'd11*', 'd12*', 
                'd13*', 'd14*', 'd15*', ],
    'gpio': [ 'e8*', 'e9*', 'e10*', 'e11*', 
                'e12*', 'e13*', 'e14*', 'e15*', 
                's0*', 's1*', 's2*', 's3*', 
                's4*', 's5*', 's6*', 's7*', ],
    'mtwi': [ 'sda*', 'scl+', ],
    'sd0': [ 'cmd*', 'clk+', 'd0*', 'd1*', 
                'd2*', 'd3*', ],
    'eint': [ '0-', '1-', '2-', ],
}

litexdict = {
    'mspi1': [ 'spimaster_clk+', 'spimaster_cs_n+', 'spimaster_mosi+', 'spimaster_miso-', ],
    'mspi0': [ 'spisdcard_clk+', 'spisdcard_cs_n+', 'spisdcard_mosi+', 'spisdcard_miso-', ],
    'vss': [ '0-', '1-', '2-', '3-', 
                '4-', ],
    'vdd': [ '0-', '1-', '2-', '3-', 
                '4-', ],
    'pwm': [ 'pwm0+', 'pwm1+', ],
    'jtag': [ 'jtag_tms-', 'jtag_tdi-', 'jtag_tdo+', 'jtag_tck+', ],
    'vdd': [ '0-', '1-', '2-', ],
    'sys': [ 'sys_clk-', 'rst-', 'pllclk-', 'sys_pll_18_o+', 
                'sys_clksel_0-', 'sys_clksel_1-', 'sys_pll_lck_o+', ],
    'uart0': [ 'uart_tx+', 'uart_rx-', ],
    'vss': [ '0-', '1-', '2-', ],
    'sdr': [ 'sdram_dm_0+', 'sdram_dq_0*', 'sdram_dq_1*', 'sdram_dq_2*', 
                'sdram_dq_3*', 'sdram_dq_4*', 'sdram_dq_5*', 'sdram_dq_6*', 
                'sdram_dq_7*', 'sdram_a_0+', 'sdram_a_1+', 'sdram_a_2+', 
                'sdram_a_3+', 'sdram_a_4+', 'sdram_a_5+', 'sdram_a_6+', 
                'sdram_a_7+', 'sdram_a_8+', 'sdram_a_9+', 'sdram_ba_0+', 
                'sdram_ba_1+', 'sdram_clock+', 'sdram_cke+', 'sdram_ras_n+', 
                'sdram_cas_n+', 'sdram_we_n+', 'sdram_cs_n+', 'sdram_a_10+', 
                'sdram_a_11+', 'sdram_a_12+', 'sdram_dm_1+', 'sdram_dq_8*', 
                'sdram_dq_9*', 'sdram_dq_10*', 'sdram_dq_11*', 'sdram_dq_12*', 
                'sdram_dq_13*', 'sdram_dq_14*', 'sdram_dq_15*', ],
    'gpio': [ 'e8*', 'e9*', 'e10*', 'e11*', 
                'e12*', 'e13*', 'e14*', 'e15*', 
                's0*', 's1*', 's2*', 's3*', 
                's4*', 's5*', 's6*', 's7*', ],
    'mtwi': [ 'i2c_sda*', 'i2c_scl+', ],
    'sd0': [ 'sdcard_cmd*', 'sdcard_clk+', 'sdcard_data0*', 'sdcard_data1*', 
                'sdcard_data2*', 'sdcard_data3*', ],
    'eint': [ '0-', '1-', '2-', ],
}

