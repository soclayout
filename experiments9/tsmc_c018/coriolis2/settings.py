# -*- Mode:Python -*-

import os
import socket
import helpers

NdaDirectory = None
if 'NDA_TOP' in os.environ:
    NdaDirectory = os.environ['NDA_TOP']
if not NdaDirectory:
    hostname = socket.gethostname()
    if hostname.startswith('lepka'):
        NdaDirectory = '/dsk/l1/jpc/crypted/soc/techno'
        if not os.path.isdir(NdaDirectory):
            print( '[ERROR] You forgot to mount the NDA '
                   'encrypted directory, stupid!' )
    else:
        NdaDirectory = '/users/soft/techno/techno'
helpers.setNdaTopDir( NdaDirectory )

import Cfg
from   Hurricane import DataBase, Cell, Instance, Net
from   CRL       import AllianceFramework
from   helpers   import overlay, l, u, n
from   NDA.node180.tsmc_c018 import (techno, FlexLib,
                                     LibreSOCIO, LibreSOCMem, pll)

techno.setup()
FlexLib.setup()
LibreSOCIO.setup()
LibreSOCMem.setup()
pll.setup()

# XXX TODO, important! fix the directions of the PLL cells
# https://gitlab.lip6.fr/vlsi-eda/coriolis/-/issues/47
def fix_pll():
    for cell in pll.getCells():
        for net in cell.getNets():
            # TODO: review
            if net.getName() == 'vdd':
                net.setType( Net.Type.POWER )
                net.setDirection( Net.Direction.IN )
            # TODO: review
            elif net.getName() == 'vss':
                net.setType( Net.Type.GROUND )
                net.setDirection( Net.Direction.IN )
            # TODO: review
            elif net.getName() == 'ck':
                net.setType( Net.Type.CLOCK )
                net.setDirection( Net.Direction.IN )
            # TODO review, should be good
            elif net.getName() in ['div_out_test', 'vco_test_ana', 'out_v']:
                net.setDirection( Net.Direction.OUT )
            # last option, set it as an input
            else:
                net.setDirection( Net.Direction.IN )

# XXX TODO uncomment this line: fix_pll()

# XXX TODO same thing for spblock_512xxxetcxxx for "q" output data

db = DataBase.getDB()
af = AllianceFramework.get()


def createBlackbox (name, libname, cellName, blackboxNames, real_name):
    global db, af
    print( '  o  Creating %s blackboxes for "ls180" design.' % name)
    rootlib  = db.getRootLibrary()
    lib      = rootlib.getLibrary( libname )
    libcell     = lib.getCell( cellName )
    if not libcell:
        raise ErrorMessage( 1, 'settings.createSramBlocks(): '
                               '%s SRAM Cell "%s" not found.' % \
                               (name, cellName) )
    libcell.setAbstractedSupply( True )
    for blackboxName in blackboxNames:
        cell     = Cell.create( lib, blackboxName )
        instance = Instance.create( cell, real_name, libcell )
        state    = af.getCatalog().getState( blackboxName, True )
        state.setCell( cell )
        state.setLogical( True )
        state.setInMemory( True )
        print( '     - {}.'.format(cell) )
        for masterNet in libcell.getNets():
            if not masterNet.isExternal():
                continue
            net = Net.create( cell, masterNet.getName() )
            net.setDirection( masterNet.getDirection() )
            net.setType( masterNet.getType() )
            net.setExternal( True )
            net.setGlobal( masterNet.isGlobal() )
            if masterNet.isSupply():
                continue
            plug = instance.getPlug( masterNet )
            plug.setNet( net )


#TODO, JP, check this, it's cut/paste and guessing
def createPLLBlackbox ():
    createBlackbox(name='PLL',
                   libname='pll',
                   cellName='pll',
                   blackboxNames = [ 'pll'
                                     ],
                   real_name='real_pll') # probably


def createSramBlackbox ():
    createBlackbox(name='SRAM',
                   libname='LibreSOCMem',
                   cellName='spblock_512w64b8w',
                   # go back to only one blackbox
                   blackboxNames = [ 'spblock_512w64b8w'
                                     ],
                   real_name='real_sram')


with overlay.CfgCache(priority=Cfg.Parameter.Priority.UserFile) as cfg:
    cfg.misc.catchCore           = False
    cfg.misc.minTraceLevel       = 12300
    cfg.misc.maxTraceLevel       = 12400
    cfg.misc.info                = False
    cfg.misc.paranoid            = False
    cfg.misc.bug                 = False
    cfg.misc.logMode             = True
    cfg.misc.verboseLevel1       = True
    cfg.misc.verboseLevel2       = True
    cfg.etesian.graphics         = 3
    cfg.etesian.spaceMargin      = 0.10
    cfg.katana.eventsLimit       = 4000000
    env = af.getEnvironment()
    env.setCLOCK( '^sys_pllclk$|^ck|^jtag_tck$' )

#with overlay.UpdateSession():
#    createSramBlackbox()
#    createPLLBlackbox ()
